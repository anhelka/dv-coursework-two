calc1_instr.e : Basic structure of calc1 instructions.
This module defines the basic structure of calc1 instrcutions, 
accoding to the design and interface specifications. 

--------------------------------------------------------------

* All instructions are defined as :

    Opcode Operand1 Operand2

* calc1 input instruction commands are:

    Code         |  0  |  1  |  2  |  3  |  4   |  5  |  6 
    Instruction  | NOP | ADD | SUB | INV | INV1 | SHL | SHR

* calc1 output response 

    Code     |      0     |     1      |                    2                    |        3
    Response | NO REPONSE | COMPLETION | INVALID CMD OR OVERFLOW/UNDERFLOW ERROR | INTERNAL EROROR

<' 

// Define legal opcodes as enumerated type 
type opcode_t: [NOP, ADD, SUB, INV, INV1, SHL, SHR] (bits:4);

struct instruction_s {
    // To indicate that specman must drive the values generated for these fields into the DUT,
    // , place a % character in front of the field name. 
    %cmd_in : opcode_t;
    %din1   : uint (bits:32);
    %din2   : uint (bits:32);

    // ! character tells specman to create an empty data structure to 
    //  hold the field name that follows. 
    !resp : uint (bits:2);
    !dout : uint (bits:32);
    
    check_response(ins : instruction_s, portnr : int) is empty;

};


extend sys {
    instrs: list of instruction_s;
}


'>