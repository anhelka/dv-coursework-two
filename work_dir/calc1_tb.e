<'
import calc1_instr; 
import calc1_DUT;
import calc1_driver;
import calc1_cover;
import calc1_checker; 

extend sys {
      
   driver : driver_u is instance; 

   setup() is also {

      set_check("...", ERROR_CONTINUE);   // don't quit the simulation on error

   }; 

   run() is also {

     simulator_command("probe -create -shm -all -depth all"); 

   };

}; // extend sys 

// extend the error handling struct

extend dut_error_struct {

   write() is only {
      out("DUT error at time ", sys.time);
      out(message);
   }; // write() is only

}; // extend dut_error

'>