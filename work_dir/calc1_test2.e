Test Scenario 1 - Normal Operations 
---------------------
1. Normal addition, subtraction, left shift, and right shift 
2. invalid commands

Main goal is to gain a high coverage rate with this test, without focus on corner cases. 

<'
import calc1_tb;

extend instruction_s {
    // test constraints 
    keep soft cmd_in == select {
        // weighted contraint 
        // more weights on arithmatic and logic commands than
        // no op and invalid commands 
        30 : [ADD, SUB, SHL, SHR];
        10 : [NOP, INV, INV1];
    };

    // drive normal cases 
    keep din1 > din2 ; 
    keep din1 > 0 ; 
    keep din2 > 0 ; 

};

extend sys {
    // generate 5 instructions 
    keep instrs.size() == 200; 
};

'>