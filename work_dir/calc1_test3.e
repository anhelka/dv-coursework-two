Test Scenario 1 - Functional Corner Cases 1
---------------------

1. Overflow and underflow error 

Main goal is to focus the test on overflow and underflow test cases and invalid commands. 

<'

import calc1_tb;
import calc1_checker; 

extend instruction_s {
    // test constraints 
    keep soft cmd_in == select {
        // weighted contraint 
        // more weights on arithmatic and logic commands than
        // no op and invalid commands 
        100 : [ADD, SUB];

    };

    // drive normal cases 
    keep din1 > 0 ; 
    keep din2 > 0 ;
    keep din1 < din2 ; 
    keep din1 + din2 > 4294967296 ; 
};

extend sys {
    // generate 100 instructions 
    keep instrs.size() == 100; 
};

'>