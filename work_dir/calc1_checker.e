<'
extend instruction_s {

    // Data checker 
    // Addition 
    when ADD'cmd_in instruction_s { 
        check_response(ins : instruction_s, portnr : int) is only {
            var sum : int = ins.din1 + ins.din2;
            if sum < 4294967296 then {
                check that ins.resp == 01;
                check that ins.dout == (ins.din1 + ins.din2) else
                dut_error(appendf("[R==>Port %s invalid output.<==R]\n \
                            Instruction %s %d %d,\n \
                            expected %032.32b \t %d,\n \
                            received %032.32b \t %d.\n",
                            portnr, 
                            ins.cmd_in, ins.din1, ins.din2, 
                            (ins.din1 + ins.din2),
                            (ins.din1 + ins.din2), 
                            ins.dout,ins.dout));
            } else {
                out("OVERFLOW");
                out();
                check that ins.resp == 02; 
            };

        };

    }; 

    // Subtraction 
    when SUB'cmd_in instruction_s { 
        check_response(ins : instruction_s, portnr : int) is only {
            var rem : int = ins.din1 - ins.din2;
            if rem > 0 then {
                check that ins.resp == 01;
                check that ins.dout == (ins.din1 - ins.din2) else
                dut_error(appendf("[R==>Port %s invalid output.<==R]\n \
                            Instruction %s %d %d,\n \
                            expected %032.32b \t %d,\n \
                            received %032.32b \t %d.\n",
                            portnr, 
                            ins.cmd_in, ins.din1, ins.din2, 
                            (ins.din1 - ins.din2),
                            (ins.din1 - ins.din2), 
                            ins.dout,ins.dout));
            } else {
                out("UNDERFLOW");
                out();
                check that ins.resp == 02; 
            };

        };
    }; 

    // Left shift
    when SHL'cmd_in instruction_s { 
        check_response(ins : instruction_s, portnr : int) is only {

            check that ins.resp == 01;
            check that ins.dout == (ins.din1 << ins.din2) else
            dut_error(appendf("[R==>Port %s invalid output.<==R]\n \
                            Instruction %s %d %d,\n \
                            expected %032.32b \t %d,\n \
                            received %032.32b \t %d.\n",
                            portnr, 
                            ins.cmd_in, ins.din1, ins.din2, 
                            (ins.din1 << ins.din2),
                            (ins.din1 << ins.din2), 
                            ins.dout,ins.dout));

        };
    }; 

    // Right shift 
    when SHR'cmd_in instruction_s { 
        check_response(ins : instruction_s, portnr : int) is only {

            check that ins.resp == 01;
            check that ins.dout == (ins.din1 >> ins.din2) else
            dut_error(appendf("[R==>Port %s invalid output.<==R]\n \
                            Instruction %s %d %d,\n \
                            expected %032.32b \t %d,\n \
                            received %032.32b \t %d.\n",
                            portnr, 
                            ins.cmd_in, ins.din1, ins.din2, 
                            (ins.din1 >> ins.din2),
                            (ins.din1 >> ins.din2), 
                            ins.dout,ins.dout));

        };
    }; 

    // No operation
    when NOP'cmd_in instruction_s { 
        check_response(ins : instruction_s, portnr : int) is only {

            check that ins.resp == 00;

        };
    }; 

    // Invalid command 
    when INV'cmd_in instruction_s { 
        check_response(ins : instruction_s, portnr : int) is only {

            check that ins.resp == 02;

        };
    }; 

    when INV1'cmd_in instruction_s { 
        check_response(ins : instruction_s, portnr : int) is only {

            check that ins.resp == 02;

        };
    }; 


};
'>