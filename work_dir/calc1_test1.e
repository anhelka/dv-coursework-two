Test constraints: extends instruction_s structure.
Test constrainsts target the specman generator to a specific 
    test described in the functional test plan.

-------------------------------------------------------------

Test Scenario 0 - TB Test
-------------------------

Test objective: create a simple test to confirm that the verification
    environment is working properly. 

Test strategy:
* Generate 5 instructions
* Use ADD command 
* Set din1 to 1
* Set din2 to 
* Send tests to each port - also tests port functionality 
 
<'

import calc1_tb;
import calc1_checker; 

extend instruction_s {
    
    // test constraints 
    keep cmd_in == ADD ;
    keep din1 == 1 ;
    keep din2 < 5 ;

};

extend sys {
    // generate 5 instructions 
    keep instrs.size() == 10; 
};

'>
