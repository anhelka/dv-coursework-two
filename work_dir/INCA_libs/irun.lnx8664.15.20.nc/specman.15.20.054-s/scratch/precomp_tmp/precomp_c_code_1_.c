/* line 7 "/home/wl17443/Documents/dv-coursework-two/work_dir/calc1_driver.e"  From 'driver_u.run' */
void r__224137__main__driver_u__run(t__main__driver_u me)
{
    r__139947__e_core__any_unit__run((t__e_core__any_unit)me);
if (sn_vs_enabled) {
    sn_vs_push_method_with_unit(91700,me->root___instance_id);
}
    if (break_on_method_call) method_debug((t__base_struct)me,91700,TRUE,0,(t__untyped)0);
    {
    /*line 8: start drive(...) */
    {
    t__main__main__driver_u__drive__224139_t frame___;
    frame___ = (t__main__main__driver_u__drive__224139_t)sn_tcm__unresolved_call(1068374,NULL,me,sn_global_method_indices[41283]);
    }
    }
    if (break_on_method_return) method_debug((t__base_struct)me,91700,FALSE,0,(t__untyped)0);
if (sn_vs_enabled) {
    sn_vs_pop_method_with_unit(91700,me->root___instance_id);
}
}

/* line 11 "/home/wl17443/Documents/dv-coursework-two/work_dir/calc1_driver.e"  From 'driver_u.drive' */
t__sn__sn_tcm_return_status r__224140__main__driver_u__drive(me,state,thread)
t__main__main__driver_u__drive__224139_t me;
int state;
t__sn__tcm_waiting_thread thread;
{
    switch (state){
        case 0: goto L0 ;
        case 1: goto L1 ;
        case 2: goto L2 ;
    } /* switch (state)... */
L0:;
{
if (sn_vs_enabled) sn_vs_tcm_push(me);
    {
    {
{
if (GLOB->sn_dbg->on_break_on_tcm_call) sn_debug_tcm(TRUE,me);
}
        {
        /*line 11: drive(...) @clk is {...} */
        if ((((me->is_very_first)) && (!((((t__main__driver_u)(me->sn_owner)) ? (sn_scheduler->tick_counter == ((t__main__driver_u)((t__main__driver_u)(me->sn_owner)))->sn___clk_occ_l) : FALSE)))))
        {
            {
            {
{
}
                {
                {
{
if (sn_session->tcm_wait_traced) {
    DISPATCH(on__tcm_wait,sn_session,session,(sn_session,me->sn_owner,"drive",1068377));
}
}
{
/*line 11: drive(...) @clk is {...} */
DISPATCH(wake_tcm_on_sync,sn_scheduler,scheduler,(sn_scheduler,44,me,thread));
thread->pc__state = 1;
return SN_ENUM(sn_tcm_return_status,wait);
L1:;
}
                }
                }
            }
            }
        } else {
{
}
        }
        }
    }
    }
    {
thread->pc__state = 2;
{
    t__main__main__driver_u__drive_reset__224142_t frame___;
    frame___ = (t__main__main__driver_u__drive_reset__224142_t)sn_tcm__unresolved_call(NULL, thread, ((t__main__driver_u)(me->sn_owner)), sn_get_method_index(41284) );
}
return SN_ENUM(sn_tcm_return_status,call);
L2:;
/*line 12: drive_reset(...) */
/*line 12: drive_reset(...) */
;
    }
if (sn_vs_enabled) sn_vs_tcm_pop(me);
}
if (GLOB->sn_dbg->on_break_on_tcm_return) sn_debug_tcm(FALSE,me);

return SN_ENUM(sn_tcm_return_status,terminate);
}
/* line 15 "/home/wl17443/Documents/dv-coursework-two/work_dir/calc1_driver.e"  From 'driver_u.drive_reset' */
t__sn__sn_tcm_return_status r__224143__main__driver_u__drive_reset(me,state,thread)
t__main__main__driver_u__drive_reset__224142_t me;
int state;
t__sn__tcm_waiting_thread thread;
{
    switch (state){
        case 0: goto L0 ;
        case 1: goto L1 ;
        case 2: goto L2 ;
    } /* switch (state)... */
L0:;
{
{
if (sn_vs_enabled) sn_vs_tcm_push(me);
    {
    {
{
if (GLOB->sn_dbg->on_break_on_tcm_call) sn_debug_tcm(TRUE,me);
}
        {
        /*line 15: drive_reset(...) @clk is {...} */
        if ((((me->is_very_first)) && (!((((t__main__driver_u)(me->sn_owner)) ? (sn_scheduler->tick_counter == ((t__main__driver_u)((t__main__driver_u)(me->sn_owner)))->sn___clk_occ_l) : FALSE)))))
        {
            {
            {
{
}
                {
                {
{
if (sn_session->tcm_wait_traced) {
    DISPATCH(on__tcm_wait,sn_session,session,(sn_session,me->sn_owner,"drive_reset",1068381));
}
}
{
/*line 15: drive_reset(...) @clk is {...} */
DISPATCH(wake_tcm_on_sync,sn_scheduler,scheduler,(sn_scheduler,46,me,thread));
thread->pc__state = 1;
return SN_ENUM(sn_tcm_return_status,wait);
L1:;
}
                }
                }
            }
            }
        } else {
{
}
        }
        }
    }
    }
/*line 16: var i : int */
/*line 16: var i : int */
 me->_i__10 = 0;
    {
    {
{
}
        {
/*line 17: i=0 */
/*line 17: i=0 */
me->_i__10=0;
        while (TRUE) {
            {
{
}
{
}
{
me->_v__224167__local__11=((t__int)(me->_i__10) <= (t__int)(8));
}
                {
                /*line 17: for {...} do {...} */
                if (!(me->_v__224167__local__11))
                {
break;
                }
                }
{
    t__untyped v__224166__local;
/*line 18: reset_p$ = 1111111 */
/*line 18: reset_p$ = 1111111 */
{
((t__e_core__session)sn_session)->curr_expr_src_ref = 1068384;
DISPATCH(do_put,(((t__main__driver_u)(me->sn_owner)))->reset_p,any_simple_port,((((t__main__driver_u)(me->sn_owner)))->reset_p,(SN_TYPE(untyped))((int)(((t__uint)(1111111))) & 127)));
}
}
                {
if (sn_session->tcm_wait_traced) {
    DISPATCH(on__tcm_wait,sn_session,session,(sn_session,me->sn_owner,"drive_reset",1068385));
}
/*line 19: wait cycle */
if (((t__main__driver_u)((t__main__driver_u)(me->sn_owner)))->sn___clk_event_struct == NULL) { 
    t__event_descriptor sn___ev =  EVENT_BY_ID(93);
     ((t__main__driver_u)((t__main__driver_u)(me->sn_owner)))->sn___clk_event_struct = DISPATCH(create_event_struct,sn___ev,event_descriptor,(sn___ev,((t__main__driver_u)(me->sn_owner))));
    }
DISPATCH(wake_tcm_on_wait_cycle,sn_scheduler,scheduler,(sn_scheduler,45,me,thread,((t__main__driver_u)((t__main__driver_u)(me->sn_owner)))->sn___clk_event_struct));
thread->pc__state = 2;
return SN_ENUM(sn_tcm_return_status,wait);
L2:;
                }
            }
        SN__LOOP__UPDATE__224168: ;
/*line 17: i+=1 */
/*line 17: i+=1 */
me->_i__10=(((int)me->_i__10) + ((int)1));
        }
        SN__LOOP__BREAK__224169: ;
        }
    }
    }
if (sn_vs_enabled) sn_vs_tcm_pop(me);
}
}
if (GLOB->sn_dbg->on_break_on_tcm_return) sn_debug_tcm(FALSE,me);

return SN_ENUM(sn_tcm_return_status,terminate);
}
/* line 5 "/home/wl17443/Documents/dv-coursework-two/work_dir/calc1_driver.e"  From 'first {...} then (...)' */
t__untyped r__224184__ee__event(t__activation_record ar) {
t__untyped sn___res;
t__main__driver_u me = (t__main__driver_u)LIST_ITEM(((t__method_frame)ar)->sn_values, 0);
t__bool detach___2;
t__long_int st______9;
l__int se______8;
t__bool ffc______7;
t__bool res______6;
l__sn__sn_event_struct sl______5;
l__bool__32 cs______4;
l__bool__32 ns______3;
t__sn__waiting_thread wt______2;
t__uint sn_new_val____11;
t__untyped v__224145__local;
t__uint sn_next_value____14;
t__uint sn_prev_value____13;
t__bool__32 v__224146__local;
t__bool__32 v__224147__local;
t__sn__sn_event_struct v__224149__local;
t__sn__event_descriptor v__224148__local;
t__sn__sn_event_struct v__224150__local;
t__sn__sn_adapter_bridge v__224151__local = 0;
/*line 5: var wt___: waiting_thread = global.scheduler.curr_wt */
wt______2 ;
wt______2=(sn_scheduler)->curr_wt;
/*line 5: var ns___: list of bool (...) = wt___.table_next_states */
ns______3 ;
ns______3=(wt______2)->table_next_states;
/*line 5: var cs___: list of bool (...) = wt___.table_curr_states */
cs______4 ;
cs______4=(wt______2)->table_curr_states;
/*line 5: var sl___: list of sn_event_struct = global.scheduler.sense */
sl______5 ;
sl______5=(sn_scheduler)->sense;
/*line 5: var res___: bool = FALSE */
res______6 ;
res______6=FALSE;
/*line 5: var ffc___: bool = global.fl.first_for_change */
ffc______7 ;
ffc______7=((GLOB)->fl)->first_for_change;
/*line 5: var se___: list of int = global.scheduler.sig_entries */
se______8 ;
se______8=(sn_scheduler)->sig_entries;
/*line 5: var st___: long_int = wt___.previous_evaluation + 1.unsafe(...) */
st______9 ;
(st______9) = (t__long_int)(((t__long_int)(wt______2)->previous_evaluation + (t__long_int)((t__long_int)1)));
/*line 5: ns___[...] = FALSE */
LIST_ITEM(ns______3,((t__int)(0))) = FALSE;
/*line 5: ns___[...] = FALSE */
LIST_ITEM(ns______3,((t__int)(1))) = FALSE;
/*line 5: ns___[...] = FALSE */
LIST_ITEM(ns______3,((t__int)(2))) = FALSE;
/*line 5: var detach___2: bool = FALSE */
detach___2 ;
detach___2=FALSE;
{
t__uint sn_new_val____11;
    /*line 5: var sn_new_val_: uint = 0.unsafe(...) */
    sn_new_val____11 ;
    sn_new_val____11=((t__uint)0);
    /*line 5: if ffc___ or (...) and not cs___[...] {...} */
    if ((ffc______7) || ((((t__long_int)((sn_simulator)->sn___SimEVent00091_occ_l) >= (t__long_int)(st______9))) && (!(LIST_ITEM(cs______4,((t__int)(2))))))) {
        /*line 5: sn_new_val_ = (...).unsafe(...) */
        {
        t__untyped v__224145__local;
((t__e_core__session)sn_session)->curr_expr_src_ref = 1068371;
        sn_new_val____11=((t__uint)((t__bit)((int)(DISPATCH1(do_get,(me)->clk_p,any_simple_port)) & 1)));
        }
    }
    /*line 5: if ffc___ {...} else {...} */
    if (ffc______7) {
        /*line 5: ns___[...] = (...).unsafe(...) */
LIST_ITEM(ns______3,((t__int)(0))) = ((t__bool__32)sn_new_val____11);
    } else {
        /*line 5: if (...) {...} else {...} */
        if (((t__long_int)((sn_simulator)->sn___SimEVent00091_occ_l) >= (t__long_int)(st______9))) {
        {
        t__uint sn_prev_value____13;
        t__uint sn_next_value____14;
            /*line 5: var sn_prev_value_: uint = cs___[...].unsafe(...) */
            sn_prev_value____13 ;
            sn_prev_value____13=((t__uint)LIST_ITEM(cs______4,((t__int)(0))));
            /*line 5: var sn_next_value_: uint = cs___[...].unsafe(...) */
            sn_next_value____14 ;
            sn_next_value____14=((t__uint)LIST_ITEM(cs______4,((t__int)(1))));
            /*line 5: if not cs___[...] {...} */
            if (!(LIST_ITEM(cs______4,((t__int)(2))))) {
                /*line 5: cs___[...]=(...).unsafe(...) */
LIST_ITEM(cs______4,((t__int)(1))) = ((t__bool__32)sn_new_val____11);
                /*line 5: sn_next_value_ = cs___[...].unsafe(...) */
                sn_next_value____14=((t__uint)LIST_ITEM(cs______4,((t__int)(1))));
                /*line 5: if sn_next_value_ == sn_prev_value_ {...} else if sn_next_value_ > sn_prev_value_ {...} else {...} */
                if (((t__uint)(sn_next_value____14) == (t__uint)(sn_prev_value____13))) {
                    /*line 5: cs___[...] = 0.unsafe(...) */
LIST_ITEM(cs______4,((t__int)(2))) = ((t__bool__32)0);
                } else {
                /*line 5: if sn_next_value_ > sn_prev_value_ {...} else {...} */
                if (((t__uint)(sn_next_value____14) > (t__uint)(sn_prev_value____13))) {
                    /*line 5: cs___[...] = 1.unsafe(...) */
LIST_ITEM(cs______4,((t__int)(2))) = ((t__bool__32)1);
                } else {
                    /*line 5: cs___[...] = (...).unsafe(...) */
LIST_ITEM(cs______4,((t__int)(2))) = ((t__bool__32)-1);
                }
                }
            }
            /*line 5: detach___2 = sn_prev_value_ > sn_next_value_ */
            detach___2=((t__uint)(sn_prev_value____13) > (t__uint)(sn_next_value____14));
            /*line 5: ns___[...] = cs___[...] */
            {
            t__bool__32 v__224146__local;
            v__224146__local=LIST_ITEM(cs______4,((t__int)(1)));
LIST_ITEM(ns______3,((t__int)(0))) = v__224146__local;
            }
        }
        } else {
            /*line 5: ns___[...] = cs___[...] */
            {
            t__bool__32 v__224147__local;
            v__224147__local=LIST_ITEM(cs______4,((t__int)(0)));
LIST_ITEM(ns______3,((t__int)(0))) = v__224147__local;
            }
        }
    }
}
/*line 5: if global.simulator.sn___SimEVent00091_event_struct == NULL {...} */
if ((((sn_simulator)->sn___SimEVent00091_event_struct) == (NULL))) {
    /*line 5: global.simulator.sn___SimEVent00091_event_struct = global.scheduler.event_map[...].create_event_struct(...) */
    {
    t__sn__event_descriptor v__224148__local;
    t__sn__sn_event_struct v__224149__local;
v__224148__local=LIST_ITEM((sn_scheduler)->event_map,((t__int)(91)));
    v__224149__local=((t__sn__sn_event_struct)SN_STRUCT_DISPATCH(create_event_struct,v__224148__local,sn__event_descriptor,
(v__224148__local,sn_simulator)));
    (sn_simulator)->sn___SimEVent00091_event_struct=v__224149__local;
    }
}
/*line 5: sl___[...] = global.simulator.sn___SimEVent00091_event_struct */
{
t__sn__sn_event_struct v__224150__local;
LIST_ITEM(sl______5,((t__int)(0))) = (sn_simulator)->sn___SimEVent00091_event_struct;
}
/*line 5: global.scheduler.within_sync = FALSE */
(sn_scheduler)->within_sync=FALSE;
    /*line 5: clk_p$ #fall 91 */
    {
    t__sn__sn_adapter_bridge v__224151__local = 0;
    v__224151__local = (SN_TYPE(sn_adapter_bridge))DISPATCH1(internal_get_adapter_struct,(me)->clk_p,any_unit);
    DISPATCH(internal_sensitize,GLOB_SIM,simulator,(GLOB_SIM,NULL,(me)->clk_p,91));
    }
/*line 5: se___[...] = global.scheduler.cur_sig_entry */
LIST_ITEM(se______8,((t__int)(0))) = ((t__int)((sn_scheduler)->cur_sig_entry));
sn___res = (t__untyped)((detach___2));
return sn___res;
}
/* line 11 "/home/wl17443/Documents/dv-coursework-two/work_dir/calc1_driver.e"  From 'first {...} then (...)' */
t__untyped r__224185__ee__sync(t__activation_record ar) {
t__untyped sn___res;
t__main__main__driver_u__drive__224139_t me = ar;
t__sn__waiting_thread v__224152__local;
t__sn__sn_event_struct v__224154__local;
t__sn__event_descriptor v__224153__local;
t__sn__sn_event_struct v__224155__local;
t__sn__sn_event_struct v__224157__local;
t__sn__event_descriptor v__224156__local;
t__sn__sn_event_struct v__224158__local;
/*line 11: res___ = FALSE */
me->_res_____15=FALSE;
/*line 11: st___ = global.scheduler.tick_counter */
(me->_st_____10) = (t__long_int)((sn_scheduler)->tick_counter);
/*line 11: sl___ = global.scheduler.sense */
me->_sl_____11=(sn_scheduler)->sense;
/*line 11: wt___ = global.scheduler.curr_wt */
{
t__sn__waiting_thread v__224152__local;
v__224152__local=(sn_scheduler)->curr_wt;
me->_wt_____12=v__224152__local;
}
/*line 11: cs___ = wt___.table_curr_states */
me->_cs_____13=(me->_wt_____12)->table_curr_states;
/*line 11: ns___ = wt___.table_next_states */
me->_ns_____14=(me->_wt_____12)->table_next_states;
/*line 11: ns___[...] = FALSE */
LIST_ITEM(me->_ns_____14,((t__int)(0))) = FALSE;
/*line 11: if cs___[...] or (...) {...} */
if ((LIST_ITEM(me->_cs_____13,((t__int)(0)))) || (((t__long_int)((((t__main__driver_u)(me->sn_owner)))->sn___clk_occ_l) >= (t__long_int)(me->_st_____10)))) {
    /*line 11: if (...) {...} else {...} */
    if (((t__long_int)((((t__main__driver_u)(me->sn_owner)))->sn___clk_occ_l) >= (t__long_int)(me->_st_____10))) {
        /*line 11: res___ = TRUE */
        me->_res_____15=TRUE;
    } else {
        /*line 11: ns___[...] = TRUE */
LIST_ITEM(me->_ns_____14,((t__int)(0))) = TRUE;
    }
}
/*line 11: if ns___[...] {...} else {...} */
if (LIST_ITEM(me->_ns_____14,((t__int)(0)))) {
    /*line 11: if sn___clk_event_struct == NULL {...} */
    if ((((((t__main__driver_u)(me->sn_owner)))->sn___clk_event_struct) == (NULL))) {
        /*line 11: sn___clk_event_struct = global.scheduler.event_map[...].create_event_struct(...) */
        {
        t__sn__event_descriptor v__224153__local;
        t__sn__sn_event_struct v__224154__local;
v__224153__local=LIST_ITEM((sn_scheduler)->event_map,((t__int)(93)));
        v__224154__local=((t__sn__sn_event_struct)SN_STRUCT_DISPATCH(create_event_struct,v__224153__local,sn__event_descriptor,
(v__224153__local,((t__main__driver_u)(me->sn_owner)))));
        (((t__main__driver_u)(me->sn_owner)))->sn___clk_event_struct=v__224154__local;
        }
    }
    /*line 11: sl___[...] = sn___clk_event_struct */
    {
    t__sn__sn_event_struct v__224155__local;
LIST_ITEM(me->_sl_____11,((t__int)(1))) = (((t__main__driver_u)(me->sn_owner)))->sn___clk_event_struct;
    }
} else {
    /*line 11: if sn___clk_event_struct == NULL {...} */
    if ((((((t__main__driver_u)(me->sn_owner)))->sn___clk_event_struct) == (NULL))) {
        /*line 11: sn___clk_event_struct = global.scheduler.event_map[...].create_event_struct(...) */
        {
        t__sn__event_descriptor v__224156__local;
        t__sn__sn_event_struct v__224157__local;
v__224156__local=LIST_ITEM((sn_scheduler)->event_map,((t__int)(93)));
        v__224157__local=((t__sn__sn_event_struct)SN_STRUCT_DISPATCH(create_event_struct,v__224156__local,sn__event_descriptor,
(v__224156__local,((t__main__driver_u)(me->sn_owner)))));
        (((t__main__driver_u)(me->sn_owner)))->sn___clk_event_struct=v__224157__local;
        }
    }
    /*line 11: sl___[...] = sn___clk_event_struct */
    {
    t__sn__sn_event_struct v__224158__local;
LIST_ITEM(me->_sl_____11,((t__int)(0))) = (((t__main__driver_u)(me->sn_owner)))->sn___clk_event_struct;
    }
}
sn___res = (t__untyped)((me->_res_____15));
return sn___res;
}
/* line 11 "/home/wl17443/Documents/dv-coursework-two/work_dir/calc1_driver.e"  From 'first {...} then (...)' */
t__untyped r__224186__ee__sync(t__activation_record ar) {
t__untyped sn___res;
t__main__main__driver_u__drive__224139_t me = ar;
t__sn__waiting_thread v__224159__local;
t__sn__sn_event_struct v__224161__local;
t__sn__event_descriptor v__224160__local;
t__sn__sn_event_struct v__224162__local;
t__sn__sn_event_struct v__224164__local;
t__sn__event_descriptor v__224163__local;
t__sn__sn_event_struct v__224165__local;
/*line 11: st___ = global.scheduler.tick_counter */
(me->_st_____10) = (t__long_int)((sn_scheduler)->tick_counter);
/*line 11: sl___ = global.scheduler.sense */
me->_sl_____11=(sn_scheduler)->sense;
/*line 11: wt___ = global.scheduler.curr_wt */
{
t__sn__waiting_thread v__224159__local;
v__224159__local=(sn_scheduler)->curr_wt;
me->_wt_____12=v__224159__local;
}
/*line 11: cs___ = wt___.table_curr_states */
me->_cs_____13=(me->_wt_____12)->table_curr_states;
/*line 11: ns___ = wt___.table_next_states */
me->_ns_____14=(me->_wt_____12)->table_next_states;
/*line 11: ns___[...] = FALSE */
LIST_ITEM(me->_ns_____14,((t__int)(0))) = FALSE;
/*line 11: if ns___[...] {...} else {...} */
if (LIST_ITEM(me->_ns_____14,((t__int)(0)))) {
    /*line 11: if sn___clk_event_struct == NULL {...} */
    if ((((((t__main__driver_u)(me->sn_owner)))->sn___clk_event_struct) == (NULL))) {
        /*line 11: sn___clk_event_struct = global.scheduler.event_map[...].create_event_struct(...) */
        {
        t__sn__event_descriptor v__224160__local;
        t__sn__sn_event_struct v__224161__local;
v__224160__local=LIST_ITEM((sn_scheduler)->event_map,((t__int)(93)));
        v__224161__local=((t__sn__sn_event_struct)SN_STRUCT_DISPATCH(create_event_struct,v__224160__local,sn__event_descriptor,
(v__224160__local,((t__main__driver_u)(me->sn_owner)))));
        (((t__main__driver_u)(me->sn_owner)))->sn___clk_event_struct=v__224161__local;
        }
    }
    /*line 11: sl___[...] = sn___clk_event_struct */
    {
    t__sn__sn_event_struct v__224162__local;
LIST_ITEM(me->_sl_____11,((t__int)(1))) = (((t__main__driver_u)(me->sn_owner)))->sn___clk_event_struct;
    }
} else {
    /*line 11: if sn___clk_event_struct == NULL {...} */
    if ((((((t__main__driver_u)(me->sn_owner)))->sn___clk_event_struct) == (NULL))) {
        /*line 11: sn___clk_event_struct = global.scheduler.event_map[...].create_event_struct(...) */
        {
        t__sn__event_descriptor v__224163__local;
        t__sn__sn_event_struct v__224164__local;
v__224163__local=LIST_ITEM((sn_scheduler)->event_map,((t__int)(93)));
        v__224164__local=((t__sn__sn_event_struct)SN_STRUCT_DISPATCH(create_event_struct,v__224163__local,sn__event_descriptor,
(v__224163__local,((t__main__driver_u)(me->sn_owner)))));
        (((t__main__driver_u)(me->sn_owner)))->sn___clk_event_struct=v__224164__local;
        }
    }
    /*line 11: sl___[...] = sn___clk_event_struct */
    {
    t__sn__sn_event_struct v__224165__local;
LIST_ITEM(me->_sl_____11,((t__int)(0))) = (((t__main__driver_u)(me->sn_owner)))->sn___clk_event_struct;
    }
}
sn___res = (t__untyped)((FALSE));
return sn___res;
}
/* line 15 "/home/wl17443/Documents/dv-coursework-two/work_dir/calc1_driver.e"  From 'first {...} then (...)' */
t__untyped r__224187__ee__sync(t__activation_record ar) {
t__untyped sn___res;
t__main__main__driver_u__drive_reset__224142_t me = ar;
t__sn__waiting_thread v__224170__local;
t__sn__sn_event_struct v__224172__local;
t__sn__event_descriptor v__224171__local;
t__sn__sn_event_struct v__224173__local;
t__sn__sn_event_struct v__224175__local;
t__sn__event_descriptor v__224174__local;
t__sn__sn_event_struct v__224176__local;
/*line 15: res___ = FALSE */
me->_res_____17=FALSE;
/*line 15: st___ = global.scheduler.tick_counter */
(me->_st_____12) = (t__long_int)((sn_scheduler)->tick_counter);
/*line 15: sl___ = global.scheduler.sense */
me->_sl_____13=(sn_scheduler)->sense;
/*line 15: wt___ = global.scheduler.curr_wt */
{
t__sn__waiting_thread v__224170__local;
v__224170__local=(sn_scheduler)->curr_wt;
me->_wt_____14=v__224170__local;
}
/*line 15: cs___ = wt___.table_curr_states */
me->_cs_____15=(me->_wt_____14)->table_curr_states;
/*line 15: ns___ = wt___.table_next_states */
me->_ns_____16=(me->_wt_____14)->table_next_states;
/*line 15: ns___[...] = FALSE */
LIST_ITEM(me->_ns_____16,((t__int)(0))) = FALSE;
/*line 15: if cs___[...] or (...) {...} */
if ((LIST_ITEM(me->_cs_____15,((t__int)(0)))) || (((t__long_int)((((t__main__driver_u)(me->sn_owner)))->sn___clk_occ_l) >= (t__long_int)(me->_st_____12)))) {
    /*line 15: if (...) {...} else {...} */
    if (((t__long_int)((((t__main__driver_u)(me->sn_owner)))->sn___clk_occ_l) >= (t__long_int)(me->_st_____12))) {
        /*line 15: res___ = TRUE */
        me->_res_____17=TRUE;
    } else {
        /*line 15: ns___[...] = TRUE */
LIST_ITEM(me->_ns_____16,((t__int)(0))) = TRUE;
    }
}
/*line 15: if ns___[...] {...} else {...} */
if (LIST_ITEM(me->_ns_____16,((t__int)(0)))) {
    /*line 15: if sn___clk_event_struct == NULL {...} */
    if ((((((t__main__driver_u)(me->sn_owner)))->sn___clk_event_struct) == (NULL))) {
        /*line 15: sn___clk_event_struct = global.scheduler.event_map[...].create_event_struct(...) */
        {
        t__sn__event_descriptor v__224171__local;
        t__sn__sn_event_struct v__224172__local;
v__224171__local=LIST_ITEM((sn_scheduler)->event_map,((t__int)(93)));
        v__224172__local=((t__sn__sn_event_struct)SN_STRUCT_DISPATCH(create_event_struct,v__224171__local,sn__event_descriptor,
(v__224171__local,((t__main__driver_u)(me->sn_owner)))));
        (((t__main__driver_u)(me->sn_owner)))->sn___clk_event_struct=v__224172__local;
        }
    }
    /*line 15: sl___[...] = sn___clk_event_struct */
    {
    t__sn__sn_event_struct v__224173__local;
LIST_ITEM(me->_sl_____13,((t__int)(1))) = (((t__main__driver_u)(me->sn_owner)))->sn___clk_event_struct;
    }
} else {
    /*line 15: if sn___clk_event_struct == NULL {...} */
    if ((((((t__main__driver_u)(me->sn_owner)))->sn___clk_event_struct) == (NULL))) {
        /*line 15: sn___clk_event_struct = global.scheduler.event_map[...].create_event_struct(...) */
        {
        t__sn__event_descriptor v__224174__local;
        t__sn__sn_event_struct v__224175__local;
v__224174__local=LIST_ITEM((sn_scheduler)->event_map,((t__int)(93)));
        v__224175__local=((t__sn__sn_event_struct)SN_STRUCT_DISPATCH(create_event_struct,v__224174__local,sn__event_descriptor,
(v__224174__local,((t__main__driver_u)(me->sn_owner)))));
        (((t__main__driver_u)(me->sn_owner)))->sn___clk_event_struct=v__224175__local;
        }
    }
    /*line 15: sl___[...] = sn___clk_event_struct */
    {
    t__sn__sn_event_struct v__224176__local;
LIST_ITEM(me->_sl_____13,((t__int)(0))) = (((t__main__driver_u)(me->sn_owner)))->sn___clk_event_struct;
    }
}
sn___res = (t__untyped)((me->_res_____17));
return sn___res;
}
/* line 15 "/home/wl17443/Documents/dv-coursework-two/work_dir/calc1_driver.e"  From 'first {...} then (...)' */
t__untyped r__224188__ee__sync(t__activation_record ar) {
t__untyped sn___res;
t__main__main__driver_u__drive_reset__224142_t me = ar;
t__sn__waiting_thread v__224177__local;
t__sn__sn_event_struct v__224179__local;
t__sn__event_descriptor v__224178__local;
t__sn__sn_event_struct v__224180__local;
t__sn__sn_event_struct v__224182__local;
t__sn__event_descriptor v__224181__local;
t__sn__sn_event_struct v__224183__local;
/*line 15: st___ = global.scheduler.tick_counter */
(me->_st_____12) = (t__long_int)((sn_scheduler)->tick_counter);
/*line 15: sl___ = global.scheduler.sense */
me->_sl_____13=(sn_scheduler)->sense;
/*line 15: wt___ = global.scheduler.curr_wt */
{
t__sn__waiting_thread v__224177__local;
v__224177__local=(sn_scheduler)->curr_wt;
me->_wt_____14=v__224177__local;
}
/*line 15: cs___ = wt___.table_curr_states */
me->_cs_____15=(me->_wt_____14)->table_curr_states;
/*line 15: ns___ = wt___.table_next_states */
me->_ns_____16=(me->_wt_____14)->table_next_states;
/*line 15: ns___[...] = FALSE */
LIST_ITEM(me->_ns_____16,((t__int)(0))) = FALSE;
/*line 15: if ns___[...] {...} else {...} */
if (LIST_ITEM(me->_ns_____16,((t__int)(0)))) {
    /*line 15: if sn___clk_event_struct == NULL {...} */
    if ((((((t__main__driver_u)(me->sn_owner)))->sn___clk_event_struct) == (NULL))) {
        /*line 15: sn___clk_event_struct = global.scheduler.event_map[...].create_event_struct(...) */
        {
        t__sn__event_descriptor v__224178__local;
        t__sn__sn_event_struct v__224179__local;
v__224178__local=LIST_ITEM((sn_scheduler)->event_map,((t__int)(93)));
        v__224179__local=((t__sn__sn_event_struct)SN_STRUCT_DISPATCH(create_event_struct,v__224178__local,sn__event_descriptor,
(v__224178__local,((t__main__driver_u)(me->sn_owner)))));
        (((t__main__driver_u)(me->sn_owner)))->sn___clk_event_struct=v__224179__local;
        }
    }
    /*line 15: sl___[...] = sn___clk_event_struct */
    {
    t__sn__sn_event_struct v__224180__local;
LIST_ITEM(me->_sl_____13,((t__int)(1))) = (((t__main__driver_u)(me->sn_owner)))->sn___clk_event_struct;
    }
} else {
    /*line 15: if sn___clk_event_struct == NULL {...} */
    if ((((((t__main__driver_u)(me->sn_owner)))->sn___clk_event_struct) == (NULL))) {
        /*line 15: sn___clk_event_struct = global.scheduler.event_map[...].create_event_struct(...) */
        {
        t__sn__event_descriptor v__224181__local;
        t__sn__sn_event_struct v__224182__local;
v__224181__local=LIST_ITEM((sn_scheduler)->event_map,((t__int)(93)));
        v__224182__local=((t__sn__sn_event_struct)SN_STRUCT_DISPATCH(create_event_struct,v__224181__local,sn__event_descriptor,
(v__224181__local,((t__main__driver_u)(me->sn_owner)))));
        (((t__main__driver_u)(me->sn_owner)))->sn___clk_event_struct=v__224182__local;
        }
    }
    /*line 15: sl___[...] = sn___clk_event_struct */
    {
    t__sn__sn_event_struct v__224183__local;
LIST_ITEM(me->_sl_____13,((t__int)(0))) = (((t__main__driver_u)(me->sn_owner)))->sn___clk_event_struct;
    }
}
sn___res = (t__untyped)((FALSE));
return sn___res;
}
/* line 5 "/home/wl17443/Documents/dv-coursework-two/work_dir/calc1_driver.e"  From 'driver_u.sn___is_quitted' */
t__bool r__224189__main__driver_u__sn___is_quitted(t__main__driver_u me)
{
t__bool result = (t__bool)0;
if (sn_vs_enabled) {
    sn_vs_push_method_with_unit(91703,me->root___instance_id);
}

    {
    /*line 5: result = me.sn___quit_occ_l != 0.unsafe(...) */
    result=(!((t__long_int)((me)->sn___quit_occ_l) == (t__long_int)(0)));
    }

if (sn_vs_enabled) {
    sn_vs_pop_method_with_unit(91703,me->root___instance_id);
}
    return result;
}

/* line 5 "/home/wl17443/Documents/dv-coursework-two/work_dir/calc1_driver.e"  From 'driver_u.sn___emit_quit' */
void r__224190__main__driver_u__sn___emit_quit(t__main__driver_u me)
{
if (sn_vs_enabled) {
    sn_vs_push_method_with_unit(91704,me->root___instance_id);
}

    {
    /*line 5: if (...) {...} */
    if (!(sn_list_key_exists((sn_scheduler)->quitted_structs,(t__untyped)(me), LD_DTYPE(2749)))) {
        /*line 5: global.scheduler.quitted_structs.add(...) */
        sn_plist_key_add((sn_scheduler)->quitted_structs,me,LD_DTYPE(2749),0);
        /*line 5: emit quit */
        {
        t__bool v__224192__local;
        t__sn__event_descriptor v__224193__local;
        t__sn__sn_event_struct v__224194__local;
        if (sn_session->in_specman_tick || sn_session->before_end_of_run || (sn_session->SN92_behavior_outside_of_specman_tick)) {
            v__224193__local = EVENT_BY_ID(92);
            if (sn_vs_enabled) {
                sn_vs_push(sn_scheduler->event_category->id, (int)DISPATCH(get_vs_id,v__224193__local,event_descriptor,(v__224193__local,me)));
            }
            v__224192__local = (sn_scheduler->tick_counter == ((t__main__driver_u)me)->sn___quit_occ_l);
            if (!v__224192__local) {
                ((t__main__driver_u)me)->sn___quit_occ_l = sn_scheduler->tick_counter;
            }
            DISPATCH1(on_quit,me,e_core__any_struct);
            if (SN_GET_ATTR(v__224193__local,EVENT_HAS_LISTENERS))
                (void)DISPATCH(call_listeners,v__224193__local,event_descriptor,(v__224193__local,me));
            if (SN_GET_ATTR(v__224193__local,EVENT_HAS_TEMP_OPERATOR))
                (void)DISPATCH(call_temp_operator,v__224193__local,event_descriptor,(v__224193__local,me));
            v__224193__local->event_count++;
            if (sn_vs_enabled) {
                DISPATCH(advance_layer_counter,v__224193__local,event_descriptor,(v__224193__local,me));
            }
            if (SN_GET_ATTR(v__224193__local,EVENT_LOG_FLAG))
                (void)DISPATCH(emit_event_callback,v__224193__local,event_descriptor,(v__224193__local,me));
            if (!v__224192__local) {
                v__224194__local = ((t__main__driver_u)me)->sn___quit_event_struct;
                if (v__224194__local==NULL) {
                     v__224194__local = DISPATCH(create_event_struct,v__224193__local,event_descriptor,(v__224193__local,me));
                    ((t__main__driver_u)me)->sn___quit_event_struct = v__224194__local;
                }
                if (v__224193__local->kind != SN_ENUM(sn_event_kind,consumed)) {
                    if (!v__224194__local->is_empty) {
                        (void)DISPATCH1(wake_waiting,v__224194__local,sn_event_struct);
                    }
                } else {
                    v__224194__local->evaluated = FALSE;
                    PLIST_ADD(sn_scheduler->event_que,v__224194__local);
                }
            }
            if (sn_vs_enabled) {
                sn_vs_pop(sn_scheduler->event_category->id, (int)DISPATCH(get_vs_id,v__224193__local,event_descriptor,(v__224193__local,me)));
            }
        } else {
            DISPATCH(emit_event_out_of_specman_tick,sn_scheduler, scheduler, (sn_scheduler,92,me));
        }
        }
        /*line 5: sn___quit(...) */
        ((void)DISPATCH(sn___quit,me,e_core__any_struct,
(me)));
    }
    }

if (sn_vs_enabled) {
    sn_vs_pop_method_with_unit(91704,me->root___instance_id);
}
}

/* line 5 "/home/wl17443/Documents/dv-coursework-two/work_dir/calc1_driver.e"  From 'driver_u.sn___clk_eval' */
void r__224191__main__driver_u__sn___clk_eval(t__main__driver_u me)
{
if (sn_vs_enabled) {
    sn_vs_push_method_with_unit(91705,me->root___instance_id);
}

    {
    /*line 5: me.sn___clk_thread = global.scheduler.event_request(...) */
    {
    t__sn__event_waiting_thread v__224195__local;
    v__224195__local=((t__sn__event_waiting_thread)SN_IPROF_NAME(r__37554__e_core__scheduler__event_request)(sn_scheduler,((t__int)(43)),((t__int)(93)),((t__int)(0)),me));
    (me)->sn___clk_thread=v__224195__local;
    }
    }

if (sn_vs_enabled) {
    sn_vs_pop_method_with_unit(91705,me->root___instance_id);
}
}

