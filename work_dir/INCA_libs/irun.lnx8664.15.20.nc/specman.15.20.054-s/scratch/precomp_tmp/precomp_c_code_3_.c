/* line 25 "/home/wl17443/Documents/dv-coursework-two/work_dir/calc1_test1.e"  From 'cmd_in' */
t__untyped r__224206__ee__un(t__activation_record ar) {
t__untyped sn___res;
t__main__instruction_s me = (t__main__instruction_s)LIST_ITEM(((t__method_frame)ar)->sn_values, 0);
sn___res = (t__untyped)((me)->cmd_in);
return sn___res;
}
/* line 26 "/home/wl17443/Documents/dv-coursework-two/work_dir/calc1_test1.e"  From 'din1' */
t__untyped r__224207__ee__un(t__activation_record ar) {
t__untyped sn___res;
t__main__instruction_s me = (t__main__instruction_s)LIST_ITEM(((t__method_frame)ar)->sn_values, 0);
sn___res = (t__untyped)((me)->din1);
return sn___res;
}
/* line 27 "/home/wl17443/Documents/dv-coursework-two/work_dir/calc1_test1.e"  From 'din2' */
t__untyped r__224208__ee__un(t__activation_record ar) {
t__untyped sn___res;
t__main__instruction_s me = (t__main__instruction_s)LIST_ITEM(((t__method_frame)ar)->sn_values, 0);
sn___res = (t__untyped)((me)->din2);
return sn___res;
}
/* line 32 "/home/wl17443/Documents/dv-coursework-two/work_dir/calc1_test1.e"  From 'instrs.size(...)' */
t__untyped r__224209__ee__un(t__activation_record ar) {
t__untyped sn___res;
t__e_core__sys me = (t__e_core__sys)LIST_ITEM(((t__method_frame)ar)->sn_values, 0);
sn___res = (t__untyped)(LIST_SIZE((me)->instrs));
return sn___res;
}
/* line 30 "/home/wl17443/Documents/dv-coursework-two/work_dir/calc1_test1.e"  From 'driver_u.root___run' */
void r__224210__main__driver_u__root___run(t__main__driver_u me)
{
    any_struct_run((t__e_core__any_struct)me);
if (sn_vs_enabled) {
    sn_vs_push_method_with_unit(91709,me->root___instance_id);
}

    {
    /*line 30: me.sn___clk_eval(...) */
    ((void)DISPATCH(sn___clk_eval,me,main__driver_u,
(me)));
    }

if (sn_vs_enabled) {
    sn_vs_pop_method_with_unit(91709,me->root___instance_id);
}
}

/* line 30 "/home/wl17443/Documents/dv-coursework-two/work_dir/calc1_test1.e"  From 'driver_u.sn___quit' */
void r__224211__main__driver_u__sn___quit(t__main__driver_u me)
{
if (sn_vs_enabled) {
    sn_vs_push_method_with_unit(91710,me->root___instance_id);
}

    {
    /*line 30: if me.sn___clk_thread != NULL then {...} */
    if ((!(((me)->sn___clk_thread) == (NULL)))) {
        /*line 30: me.sn___clk_thread.quit_tick = global.scheduler.tick_counter */
        (((me)->sn___clk_thread)->quit_tick) = (t__long_int)((sn_scheduler)->tick_counter);
    }
    }

if (sn_vs_enabled) {
    sn_vs_pop_method_with_unit(91710,me->root___instance_id);
}
}

/* line 30 "/home/wl17443/Documents/dv-coursework-two/work_dir/calc1_test1.e"  From 'driver_u.sn___internal_quit' */
void r__224212__main__driver_u__sn___internal_quit(t__main__driver_u me)
{
if (sn_vs_enabled) {
    sn_vs_push_method_with_unit(91711,me->root___instance_id);
}

    {
    /*line 30: me.sn___clk_thread = NULL */
    {
    t__sn__event_waiting_thread v__224214__local;
    v__224214__local=NULL;
    (me)->sn___clk_thread=v__224214__local;
    }
    }

if (sn_vs_enabled) {
    sn_vs_pop_method_with_unit(91711,me->root___instance_id);
}
}

/* line 30 "/home/wl17443/Documents/dv-coursework-two/work_dir/calc1_test1.e"  From 'driver_u.sn___reset_events' */
void r__224213__main__driver_u__sn___reset_events(t__main__driver_u me)
{
if (sn_vs_enabled) {
    sn_vs_push_method_with_unit(91712,me->root___instance_id);
}

    {
    /*line 30: sn___clk_event_struct = NULL */
    {
    t__sn__sn_event_struct v__224215__local;
    v__224215__local=NULL;
    (me)->sn___clk_event_struct=v__224215__local;
    }
    }

if (sn_vs_enabled) {
    sn_vs_pop_method_with_unit(91712,me->root___instance_id);
}
}

