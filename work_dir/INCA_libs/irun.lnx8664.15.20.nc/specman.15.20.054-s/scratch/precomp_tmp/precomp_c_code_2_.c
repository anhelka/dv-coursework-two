/* line 10 "/home/wl17443/Documents/dv-coursework-two/work_dir/calc1_tb.e"  From 'sys.setup' */
void r__224201__e_core__sys__setup(t__e_core__sys me)
{
if (sn_vs_enabled) {
    sn_vs_push_method_with_unit(91706,me->root___instance_id);
}
    if (break_on_method_call) method_debug((t__base_struct)me,91706,TRUE,0,(t__untyped)0);
    {
    /*line 12: set_check(...) */
    ((void)DISPATCH(set_check,GLOB,e_core__global,
(GLOB,"...",3,NULL,TRUE)));
    }
    if (break_on_method_return) method_debug((t__base_struct)me,91706,FALSE,0,(t__untyped)0);
if (sn_vs_enabled) {
    sn_vs_pop_method_with_unit(91706,me->root___instance_id);
}
}

/* line 16 "/home/wl17443/Documents/dv-coursework-two/work_dir/calc1_tb.e"  From 'sys.run' */
void r__224202__e_core__sys__run(t__e_core__sys me)
{
    r__139948__e_core__sys__run((t__e_core__any_unit)me);
if (sn_vs_enabled) {
    sn_vs_push_method_with_unit(91707,me->root___instance_id);
}
    if (break_on_method_call) method_debug((t__base_struct)me,91707,TRUE,0,(t__untyped)0);
    {
    /*line 18: simulator_command(...) */
    ((void)DISPATCH(simulator_command,GLOB,e_core__global,
(GLOB,"probe -create -shm -all -depth all")));
    }
    if (break_on_method_return) method_debug((t__base_struct)me,91707,FALSE,0,(t__untyped)0);
if (sn_vs_enabled) {
    sn_vs_pop_method_with_unit(91707,me->root___instance_id);
}
}

/* line 28 "/home/wl17443/Documents/dv-coursework-two/work_dir/calc1_tb.e"  From 'dut_error_struct.write' */
void r__224203__sn__dut_error_struct__write(t__sn__dut_error_struct me)
{
if (sn_vs_enabled) {
    sn_vs_push_method(91708);
}
    if (break_on_method_call) method_debug((t__base_struct)me,91708,TRUE,0,(t__untyped)0);
    {
    /*line 29: out(...) */
    sn_out_opt(sn_append_strings(2,"DUT error at time ",any_to_string((SN_TYPE(untyped))(((GLOB)->sys)->time), 1387)));
    /*line 30: out(...) */
    sn_out_opt((me)->message);
    }
    if (break_on_method_return) method_debug((t__base_struct)me,91708,FALSE,0,(t__untyped)0);
if (sn_vs_enabled) {
    sn_vs_pop_method(91708);
}
}

