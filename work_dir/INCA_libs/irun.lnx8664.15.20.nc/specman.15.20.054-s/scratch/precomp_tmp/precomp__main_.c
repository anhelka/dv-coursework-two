/* This is an automatically generated main for specman calc1_test1 */
#include <stdlib.h>
#include <string.h>
#include <stdio.h>

int sn_developer_mode = 0;
#ifdef __cplusplus
extern "C" void init_the_global_struct(int);
extern "C" char* normal_screen_prompt(char*, unsigned int);
extern "C" void init0__calc1_test1(void);
extern "C" void main_loop(char *, char *, char *, char *, char *, int, char * []);
#else /* __cplusplus */
extern void init_the_global_struct(int);
extern char* normal_screen_prompt(char*, unsigned int);
extern void init0__calc1_test1(void);
extern void main_loop(char *, char *, char *, char *, char *, int, char * []);
#endif
#ifdef __cplusplus
void specman_main(int argc, char *argv[])
#else /* __cplusplus */
void specman_main(argc, argv) int argc; char **argv;
#endif
{
    static int second_pass = 0;
    extern int sn_currently_in_simulator__;
    char *devmode;char *compiler;
    if (second_pass) return; else second_pass = 1; 
    if (sn_currently_in_simulator__) abort ();
    init_the_global_struct(2320);
    devmode = getenv("SPECMAN_DEVELOPER_MODE");
    compiler = getenv("SPECMAN_CCOMPILER");
    if (devmode && strcmp(devmode, "NONE") && !(compiler && !strstr(compiler, "gcc"))) sn_developer_mode = 1;
    init0__calc1_test1(); 
    main_loop("precomp", "Fri Dec 20 12:29:22 2019",
        "Specman(64) Elite (15.20.054-s Build 236)  -  Linked on Fri Jun  8 23:27:48 2018", "", "%s(64) calc1_test1 (%s%s)  %s-  Linked on Fri Dec 20 12:29:22 2019", argc, argv);
}

