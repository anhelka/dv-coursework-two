SN_EXTERNC void init0__calc1_DUT(void);
SN_EXTERNC void rp_init0__224135(void);
SN_EXTERNC void r__224136__ee__ee_init0(void);

void init0__calc1_driver(void)
{
    init0__calc1_DUT();
rp_init0__224135();
r__224136__ee__ee_init0();
}

void rp_init0__224135(void) {
    init_routine_pointer((t__routine_pointer)r__224137__main__driver_u__run);
    init_routine_pointer((t__routine_pointer)r__224140__main__driver_u__drive);
    init_routine_pointer((t__routine_pointer)r__224143__main__driver_u__drive_reset);
    init_routine_pointer((t__routine_pointer)r__224189__main__driver_u__sn___is_quitted);
    init_routine_pointer((t__routine_pointer)r__224190__main__driver_u__sn___emit_quit);
    init_routine_pointer((t__routine_pointer)r__224191__main__driver_u__sn___clk_eval);
}

void r__224136__ee__ee_init0(void) {
    sn_addEvalExplicitFunc(r__224184__ee__event);
    sn_addEvalExplicitFunc(r__224185__ee__sync);
    sn_addEvalExplicitFunc(r__224186__ee__sync);
    sn_addEvalExplicitFunc(r__224187__ee__sync);
    sn_addEvalExplicitFunc(r__224188__ee__sync);
}

