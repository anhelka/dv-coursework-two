<'
import calc1_tb; 
import calc1_checker; 

extend instruction_s {
    // test constraints 
    keep soft cmd_in == select {
        // weighted contraint 
        // more weights on arithmatic and logic commands than
        // no op and invalid commands 
        100 : [SHL, SHR];

    };

    // drive normal cases 
    keep din1 > 0 ;
    keep din2 > 0;  
    keep din2 <= 32 ;
};

extend sys {
    keep instrs.size() == 100; 
};
'>