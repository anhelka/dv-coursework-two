<'

extend driver_u {

   run() is also {
      start drive();
   };

   drive() @clk is {
      var portnr : int = 0; 
      for portnr from 1 to 4 do {
         drive_reset();
         for each (ins) in sys.instrs do {
            drive_instrs(ins, index, portnr);
            collect_response(ins, portnr);
            ins.check_response(ins, portnr);
            wait cycle;
         };
      };
      wait [10] * cycle;
      stop_run();
   };

   drive_reset() @clk is {
      var i : int;
      for { i=0; i<=8; i+=1 } do {
         reset_p$ = 1111111;
         wait cycle;
      };
      reset_p$ = 0000000;
   }; 

   drive_instrs(ins : instruction_s, i : int, portnr: int) @clk is {
      // display generated command and data
      outf("Port# = %s, test# %s, command = %s\n", portnr, i,ins.cmd_in);
      out("Op1     = ", ins.din1);
      out("Op2     = ", ins.din2);
      out();

      case portnr {
         1:{
            // drive data into calculator port 1
            req1_cmd_in_p$  = pack(NULL, ins.cmd_in);
            req1_data_in_p$ = pack(NULL, ins.din1);

            wait cycle;

            req1_cmd_in_p$  = 0000;  
            req1_data_in_p$ = pack(NULL, ins.din2);
         };
         2:{
            // drive data into calculator port 1
            req2_cmd_in_p$  = pack(NULL, ins.cmd_in);
            req2_data_in_p$ = pack(NULL, ins.din1);

            wait cycle;

            req2_cmd_in_p$  = 0000;  
            req2_data_in_p$ = pack(NULL, ins.din2);
         };
         3:{
            // drive data into calculator port 3
            req3_cmd_in_p$  = pack(NULL, ins.cmd_in);
            req3_data_in_p$ = pack(NULL, ins.din1);
               
            wait cycle;

            req3_cmd_in_p$  = 0000;  
            req3_data_in_p$ = pack(NULL, ins.din2);
         };
         4:{
            // drive data into calculator port 4
            req4_cmd_in_p$  = pack(NULL, ins.cmd_in);
            req4_data_in_p$ = pack(NULL, ins.din1);
               
            wait cycle;

            req4_cmd_in_p$  = 0000;  
            req4_data_in_p$ = pack(NULL, ins.din2);
         };
      };
   };

   collect_response(ins : instruction_s, portnr : int) @clk is {
      case portnr {
         1:{
            wait @resp1 or [5] * cycle;
            ins.resp = out_resp1_p$;
            ins.dout = out_data1_p$;
         };
         2:{
            wait @resp2 or [5] * cycle;
            ins.resp = out_resp2_p$;
            ins.dout = out_data2_p$;
         };
         3:{
            wait @resp3 or [5] * cycle;
            ins.resp = out_resp3_p$;
            ins.dout = out_data3_p$;
         };
         4:{
            wait @resp4 or [5] * cycle; 
            ins.resp = out_resp4_p$;
            ins.dout = out_data4_p$;
         };
      };
   };
};

'>